# tet-rs

A Tetris clone in Rust.

This project is designed to support multiple front-ends, currently implementing
only a TUI interface.

## Compilation

`tet-rs` currently requires the nightly compiler for `nll` and `const_fn` support.

## Default keybindings

Action | Primary | Secondary
-------|---------|----------
Move   | Arrows  | WASD
Turn   | z / x   | , / .
Quit   | q       | Ctrl-C
