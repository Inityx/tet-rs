#![feature(array_value_iter)]
#![warn(clippy::all)]

mod tet;
mod runner;

use std::{time::Duration, io};

use self::{
    tet::{
        game::Game,
        geometry::coord::Coord,
    },
    runner::term::interface::Interface,
};

const DIMENSIONS: Coord<u16> = Coord(10, 18);
const INTERVAL: Duration = Duration::from_millis(1000);

fn main() -> io::Result<()> {
    let game = Game::new(DIMENSIONS, INTERVAL);
    let score = Interface::play(game)?;
    print!("\nCongratulations! You got {} points.\n\n", score);
    Ok(())
}
