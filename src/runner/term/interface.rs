use std::{
    sync::{Arc, Mutex, mpsc},
    io,
};

use super::render::Renderer;
use crate::tet::game::{self, Game};

#[derive(Debug)]
enum Event { Step, Quit, Move(game::Move) }

type GameLock = Arc<Mutex<Game>>;

pub struct Interface(pub io::Stdout);

impl Interface {
    pub fn play(game: Game) -> io::Result<u32> {
        // shared memory
        let game = Arc::new(Mutex::new(game));
        let (event_tx, event_rx) = mpsc::channel();

        // Workers
        let input_thread = input_thread(event_tx.clone());
        let step_thread = step_thread(event_tx, Arc::clone(&game));

        // Game loop
        Interface(io::stdout())
            .game_loop(
                Arc::clone(&game),
                event_rx,
            )?;

        eprintln!("Finished game loop");
        
        input_thread.join().unwrap();
        step_thread.join().unwrap();
        
        let game = game.lock().unwrap();
        Ok(game.score())
    }

    fn game_loop(&mut self, game: GameLock, event_rx: mpsc::Receiver<Event>) -> io::Result<()> {
        let Interface(stdout) = self;
        let mut renderer = Renderer::new(stdout.lock())?;

        // Initial render
        renderer.render(&game.lock().unwrap())?;

        'event_loop: loop {
            let event = event_rx.recv().expect("Event loop channel has hung up");

            {
                let mut game = game.lock().unwrap();

                let status = match event {
                    Event::Step    => game.step(),
                    Event::Move(i) => game.do_move(i),
                    Event::Quit    => break 'event_loop,
                };

                match status {
                    game::Status::Playing => renderer.render(&game)?,
                    game::Status::Cleared(lines) => {
                        renderer.line_clear(&game, lines)?;
                        renderer.render(&game)?;
                    },
                    game::Status::Lose => break 'event_loop,
                }
            }
        }
        
        drop(game);
        drop(event_rx);
        Ok(())
    }
}

use std::thread::{sleep, spawn, JoinHandle};

fn input_thread(event_queue: mpsc::Sender<Event>) -> JoinHandle<()> {
    use termion::input::TermRead;

    fn key_to_event(key: ::termion::event::Key) -> Option<Event> {
        use self::{Event::*, game::Move::*};
        use termion::event::Key::*;

        let event = match key {
            Char('w') | Up        => Move(Slam),
            Char('s') | Down      => Move(MoveD),
            Char('a') | Left      => Move(MoveL),
            Char('d') | Right     => Move(MoveR),
            Char(',') | Char('z') => Move(TurnL),
            Char('.') | Char('x') => Move(TurnR),
            Char('q') | Ctrl('c') => Quit,
            _ => return None,
        };

        Some(event)
    }

    spawn(move || {
        let tty = ::termion::get_tty().unwrap();

        for key in tty.keys() {
            if let Ok(key) = key {
                if let Some(event) = key_to_event(key) {
                    if event_queue.send(event).is_err() { break; }
                }
            };
        }

        eprintln!("Exiting event thread");
    })
}

fn step_thread(event_queue: mpsc::Sender<Event>, game: GameLock) -> JoinHandle<()> {
    spawn(move || {
        loop {
            let interval = game.lock().unwrap().interval();
            sleep(interval);
            if event_queue.send(Event::Step).is_err() { break; }
        }

        eprintln!("Exiting step thread");
    })
}
