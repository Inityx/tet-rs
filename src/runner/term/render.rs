use std::io::{self, StdoutLock, Write};
use arrayvec::ArrayVec;
use crate::tet::{
    game::Game,
    geometry::coord::Coord, tetrimino::BLOCK_COUNT,
};

pub struct Renderer<'a> {
    stdout: termion::raw::RawTerminal<StdoutLock<'a>>,
    term_size: Coord<u16>,
    board_start: Coord<u16>,
}

impl<'a> ::std::ops::Drop for Renderer<'a> {
    fn drop(&mut self) {
        write!(self.stdout, "{}", termion::cursor::Show).unwrap();
    }
}

const BLOCK_FILL:  &str = "[]";
const BLOCK_BLANK: &str = "  ";

impl<'a> Renderer<'a> {
    pub fn new(stdout: StdoutLock<'a>) -> io::Result<Self> {
        use termion::raw::IntoRawMode;

        let mut stdout = stdout.into_raw_mode()?;
        write!(stdout, "{}", termion::cursor::Hide)?;
        
        Ok(Renderer {
            stdout,
            term_size: Default::default(),
            board_start: Default::default(),
        })
    }

    // Utility
    fn too_small_for(&self, board_size: Coord<u16>) -> bool {
        let Coord(term_x, term_y) = self.term_size;
        let Coord(reqd_x, reqd_y) = board_size * Coord(2, 1) + Coord(2, 3);
        term_x < reqd_x || term_y < reqd_y
    }

    fn resize(&mut self, term_size: Coord<u16>, board_size: Coord<u16>) -> bool {
        if term_size == self.term_size { return false; }

        self.term_size = term_size;
        if self.too_small_for(board_size) { panic!("Term too small :(") }
        self.board_start = (term_size / 2) - (board_size / Coord(1, 2));

        true
    }

    fn clear_all(&mut self) -> io::Result<()> {
        write!(self.stdout, "{}", termion::clear::All)
    }

    fn reset_colors(&mut self) -> io::Result<()> {
        use termion::color::{Fg, Bg, Reset};
        write!(self.stdout, "{}{}", Fg(Reset), Bg(Reset))
    }

    fn move_cursor(&mut self, coord: Coord<u16>) -> io::Result<()> {
        let Coord(x, y) = Coord(1, 1) + coord; // Goto is 1-based
        let location = termion::cursor::Goto(x, y);
        write!(self.stdout, "{}", location)
    }

    // Rendering routines
    fn draw_board(&mut self, game: &Game) -> io::Result<()> {
        use crate::tet::tetrimino::Tetrimino;
        // Terminal is upside down from board coords
        
        // Draw board
        for (i, row) in game.board().into_iter().rev().enumerate() {
            self.move_cursor(self.board_start + Coord(0, i as u16))?;

            for block in row {
                if let Some(color) = block {
                    write!(self.stdout, "{}{}", color::Pair::from(*color), BLOCK_FILL)?;
                } else {
                    self.reset_colors()?;
                    write!(self.stdout, "{}", BLOCK_BLANK)?;
                };
            }
        }

        let Coord(_, height) = game.board().size();

        // Draw ghost
        if let Some(ghost) = game.cursor_projected() {
            let Tetrimino(_, blocks, _) = ghost.to_global();

            for Coord(x, y) in blocks {
                self.move_cursor(self.board_start + Coord(x * 2, height - y - 1))?;
                write!(self.stdout, "{}{}", *color::GHOST, BLOCK_FILL)?;
            }
        }

        // Draw cursor
        if let Some(cursor) = game.cursor() {
            let Tetrimino(_, blocks, color) = cursor.to_global();

            for Coord(x, y) in blocks {
                self.move_cursor(self.board_start + Coord(x * 2, height - y - 1))?;
                write!(self.stdout, "{}{}", color::Pair::from(color), BLOCK_FILL)?;
            }
        }

        Ok(())
    }

    fn draw_score(&mut self, game: &Game) -> io::Result<()> {
        let Coord(_, board_height) = game.board().size();
        let score = game.score();
        let start = self.board_start + Coord(1, board_height + 1);
        self.move_cursor(start)?;
        self.reset_colors()?;
        write!(self.stdout, "{}", score)
    }

    fn draw_interface(&mut self, game: &Game) -> io::Result<()> {
        let Coord(w, h) = game.board().size() * Coord(2, 1);
        self.reset_colors()?;

        // Draw top
        self.move_cursor(self.board_start - Coord(0, 1))?;
        for _ in 0..w {
            write!(self.stdout, "═")?;
        }

        // Draw bottom
        self.move_cursor(self.board_start + Coord(0, h))?;
        for _ in 0..w {
            write!(self.stdout, "═")?;
        }

        // Draw left
        self.move_cursor(self.board_start - Coord(1, 0))?;
        for _ in 0..h {
            write!(self.stdout, "║\n\x08")?; // pipe newline backspace
        }

        // Draw right
        self.move_cursor(self.board_start + Coord(w, 0))?;
        for _ in 0..h {
            write!(self.stdout, "║\n\x08")?; // pipe newline backspace
        }

        // Draw corners
        self.move_cursor(self.board_start - Coord(1, 1)              )?; write!(self.stdout, "╔")?;
        self.move_cursor(self.board_start - Coord(1, 0) + Coord(0, h))?; write!(self.stdout, "╚")?;
        self.move_cursor(self.board_start - Coord(0, 1) + Coord(w, 0))?; write!(self.stdout, "╗")?;
        self.move_cursor(self.board_start               + Coord(w, h))?; write!(self.stdout, "╝")?;

        Ok(())
    }

    // Render
    pub fn render(&mut self, game: &Game) -> io::Result<()> {
        let board_size = game.board().size();
        let term_size = {
            let (width, height) = termion::terminal_size()?;
            Coord(width, height)
        };

        if self.resize(term_size, board_size) {
            self.clear_all()?;
            self.draw_interface(game)?;
        }

        self.draw_board(game)?;
        self.draw_score(game)?;
        self.stdout.flush()?;
        
        Ok(())
    }

    pub fn line_clear(&mut self, game: &Game, lines: ArrayVec<[u16; BLOCK_COUNT]>) -> io::Result<()> {
        use std::thread::sleep;
        let Coord(width, height) = game.board().size(); 

        write!(self.stdout, "{}", *color::WHITE)?;
        for i in lines {
            self.move_cursor(self.board_start + Coord(0, height - i - 1))?;
            write!(self.stdout, "{: >count$}", "", count = width as usize * 2)?;
        }

        self.stdout.flush()?;
        sleep(game.interval() / 4);

        Ok(())
    }
}

mod color {
    use termion::color::{Fg, Bg, Rgb};
    use lazy_static::lazy_static;
    use crate::tet;

    type Color24 = (u8, u8, u8);
    
    fn fg(color: tet::Color) -> Color24 {
        use self::tet::Color::*;
        match color {
            Red    => (0xef, 0x29, 0x29),
            Orange => (0xfc, 0xaf, 0x3e),
            Yellow => (0xfc, 0xe9, 0x4f),
            Green  => (0x8a, 0xe2, 0x34),
            Cyan   => (0x5e, 0xc0, 0xc1),
            Blue   => (0x72, 0x9f, 0xcf),
            Purple => (0xad, 0x7f, 0xa8),
        }
    }
    
    fn bg(color: tet::Color) -> Color24 {
        use self::tet::Color::*;
        match color {
            Red    => (0xa4, 0x00, 0x00),
            Orange => (0xce, 0x5c, 0x00),
            Yellow => (0xc4, 0xa0, 0x00),
            Green  => (0x4e, 0x9a, 0x06),
            Cyan   => (0x33, 0x89, 0x8f),
            Blue   => (0x34, 0x65, 0xa4),
            Purple => (0x75, 0x50, 0x7b),
        }
    }

    pub struct Pair(Fg<Rgb>, Bg<Rgb>);

    impl From<(Color24, Color24)> for Pair {
        fn from(((fr, fg, fb), (br, bg, bb)): (Color24, Color24)) -> Self {
            Pair(Fg(Rgb(fr, fg, fb)), Bg(Rgb(br, bg, bb)))
        }
    }

    impl From<tet::Color> for Pair {
        fn from(color: tet::Color) -> Self {
            Pair::from((fg(color), bg(color)))
        }
    }

    impl ::std::fmt::Display for Pair {
        fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
            let Pair(fg, bg) = self;
            write!(fmt, "{}{}", fg, bg)
        }
    }

    lazy_static! {
        pub static ref WHITE: Pair = Pair::from((
            (0xff, 0xff, 0xff),
            (0xff, 0xff, 0xff),
        ));
        pub static ref GHOST: Pair = Pair::from((
            (0x10, 0x10, 0x10),
            (0x40, 0x40, 0x40),
        ));
    }
}
