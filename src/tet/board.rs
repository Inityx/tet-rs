use std::iter;

use crate::tet::{
    Color,
    geometry::coord::Coord,
    tetrimino::BLOCK_COUNT,
};

use ::arrayvec::ArrayVec;

pub struct Board {
    size: Coord<u16>,
    rows: Vec<Row>,
}

impl Board {
    pub fn new(Coord(width, height): Coord<u16>) -> Board {
        let rows = {
            let row = Row::new(width as usize);
            iter::repeat(row)
                .take(height as usize)
                .collect::<Vec<_>>()
        };
        
        Board { size: Coord(width, height), rows }
    }

    pub fn size(&self) -> Coord<u16> { self.size }

    pub fn clear_full_rows(&mut self) -> ArrayVec<[u16; BLOCK_COUNT]> {
        let Coord(width, _): Coord<usize> = self.size.into();

        let full_idx = self.rows.iter()
            .enumerate()
            .filter(|(_, row)| row.is_full())
            .map(|(i, _)| i as u16)
            .collect::<ArrayVec<_>>();
        
        // Reverse order to preserve indexes
        for i in full_idx.iter().rev() {
            self.rows.remove(*i as usize);
            self.rows.push(Row::new(width));
        }

        full_idx
    }
}

impl<'a> IntoIterator for &'a Board {
    type Item     = <&'a [Row] as IntoIterator>::Item;
    type IntoIter = <&'a [Row] as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        self.rows.iter()
    }
}

use std::ops::{Index, IndexMut};

impl Index<u16> for Board {
    type Output = Row;
    
    fn index(&self, i: u16) -> &Self::Output {
        &self.rows[i as usize]
    }
}

impl IndexMut<u16> for Board {
    fn index_mut(&mut self, i: u16) -> &mut Self::Output {
        &mut self.rows[i as usize]
    }
}

impl Index<Coord<u16>> for Board {
    type Output = Option<Color>;
    
    fn index(&self, Coord(col, row): Coord<u16>) -> &Self::Output {
        &self.rows[row as usize][col as usize]
    }
}

impl IndexMut<Coord<u16>> for Board {
    fn index_mut(&mut self, Coord(col, row): Coord<u16>) -> &mut Self::Output {
        &mut self.rows[row as usize][col as usize]
    }
}

#[derive(Clone)]
pub struct Row(Box<[Option<Color>]>);

impl Row {
    pub fn new(width: usize) -> Row {
        let squares = iter::repeat(None)
            .take(width)
            .collect::<Vec<_>>();

        Row(squares.into_boxed_slice())
    }

    pub fn is_full(&self) -> bool {
        self.into_iter().all(Option::is_some)
    }
}

impl<'a> IntoIterator for &'a Row {
    type Item     = <&'a [Option<Color>] as IntoIterator>::Item;
    type IntoIter = <&'a [Option<Color>] as IntoIterator>::IntoIter;

    fn into_iter(self) -> Self::IntoIter {
        let Row(blocks) = self;
        blocks.iter()
    }
}

impl Index<usize> for Row {
    type Output = Option<Color>;
    
    fn index(&self, i: usize) -> &Self::Output {
        let Row(squares) = self;
        &squares[i]
    }
}

impl IndexMut<usize> for Row {
    fn index_mut(&mut self, i: usize) -> &mut Self::Output {
        let Row(squares) = self;
        &mut squares[i]
    }
}
