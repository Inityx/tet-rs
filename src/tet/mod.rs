pub mod tetrimino;
pub mod geometry;
pub mod board;
pub mod game;

#[derive(Clone, Copy)]
pub enum Color {
    Red,
    Orange,
    Yellow,
    Green,
    Cyan,
    Blue,
    Purple,
}
