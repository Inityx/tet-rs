use super::{
    tetrimino::{self, Tetrimino, Piece, Template},
    board::Board,
    geometry::{
        coord::Coord,
        rotate::Rotation,
    },
};

use std::time::Duration;
use ::arrayvec::ArrayVec;
use tetrimino::BLOCK_COUNT;

const SCORE_MULTIPLIER: u32 = 100;

pub struct Game {
    board: Board,
    interval: Duration,
    score: u32,
    cursor: Option<Cursor>,
}

impl Game {
    pub fn new(size: Coord<u16>, interval: Duration) -> Self {
        Game {
            board: Board::new(size),
            interval,
            score: 0,
            cursor: None,
        }
    }

    // Game mutators
    fn refill_cursor(&mut self, template: &'static Template) {
        if self.cursor.is_some() { panic!("Tried to refill `Some` cursor"); }

        let top = {
            let Coord(width, height) = self.board.size();
            Coord(width / 2, height - 1)
        };

        self.cursor = Some(Cursor(
            template,
            Rotation::default(),
            top
        ));
    }

    fn place_cursor(&mut self) {
        let Tetrimino(_, blocks, color) = self
            .cursor
            .take()
            .expect("Tried to place `None` cursor")
            .to_global();
        
        for coord in blocks {
            self.board[coord] = Some(color)
        }
    }

    fn this_cursor_overlap(&self, cursor: Cursor) -> bool {
        let Tetrimino(_, blocks, _) = cursor.to_global();
        blocks.into_iter().any(|coord| self.board[coord].is_some())
    }

    fn this_cursor_oob(&self, cursor: Cursor) -> bool {
        let Coord(w, h) = self.board.size();
        let Tetrimino(_, blocks, _) = cursor.to_global();

        blocks.into_iter().any(|Coord(x, y)| x >= w || y >= h)
    }

    /// Returns `Some` if valid translation, `None` if out of bounds or overlap
    fn this_cursor_translated(
        &self,
        Cursor(template, rotation, location): Cursor,
        offset: Coord<i16>
    ) -> Option<Cursor> {
        let location = Coord::<i16>::from(location) + offset;
        let cursor = Cursor(template, rotation, location.into());

        if self.this_cursor_oob(cursor) || self.this_cursor_overlap(cursor) {
            return None;
        }

        Some(cursor)
    }

    fn cursor_translated(&self, offset: Coord<i16>) -> Option<Cursor> {
        self.cursor.and_then(|c| self.this_cursor_translated(c, offset))
    }

    /// Returns `Some` if valid rotation, `None` if out of bounds or overlap
    fn cursor_rotated(
        &self,
        offset: fn(Rotation) -> Rotation
    ) -> Option<Cursor> {
        self.cursor.and_then(|Cursor(template, rotation, location)| {
            let cursor = Cursor(template, offset(rotation), location);

            if !(self.this_cursor_oob(cursor) || self.this_cursor_overlap(cursor)) {
                return Some(cursor);
            }

            // Attempt adjusting all directions by 3
            const OFFSETS: &[Coord<i16>] = &[Coord(1, 0), Coord(-1, 0), Coord(0, -1), Coord(0, 1)];
            for offset in OFFSETS {
                for i in 1..=3 {
                    let cursor = self.this_cursor_translated(cursor, *offset * i);
                    if cursor.is_some() { return cursor; }
                }
            }

            None
        })
    }

    fn clear_full_rows(&mut self) -> Status {
        let cleared = self.board.clear_full_rows();
        let count = cleared.len() as u32;

        if count > 0 {
            self.score += (count * count) * SCORE_MULTIPLIER;
            return Status::Cleared(cleared);
        }

        Status::Playing
    }

    pub fn step(&mut self) -> Status {
        if self.cursor.is_none() {
            self.interval -= Duration::from_millis(5);
            self.refill_cursor(tetrimino::random());

            let new_cursor_overlap = self.this_cursor_overlap(self.cursor.unwrap());
            return if new_cursor_overlap { Status::Lose } else { Status::Playing };
        }
        
        self.do_move(Move::MoveD)
    }

    // Accessors
    pub fn board   (&self) -> &Board          { &self.board }
    pub fn score   (&self) -> u32             { self.score }
    pub fn interval(&self) -> Duration        { self.interval }
    pub fn cursor  (&self) -> Option<Cursor>  { self.cursor }

    pub fn cursor_projected(&self) -> Option<Cursor>  {
        self.cursor.map(|mut cursor| {
            while let Some(new_cursor) = self.this_cursor_translated(cursor, Coord(0, -1)) {
                cursor = new_cursor;
            }

            cursor
        })
    }

    // Cursor controls
    pub fn do_move(&mut self, input: Move) -> Status {
        if self.cursor.is_none() { return Status::Playing }

        use self::Move::*;

        let new_cursor = match input {
            MoveL => self.cursor_translated(Coord(-1, 0)),
            MoveR => self.cursor_translated(Coord( 1, 0)),
            TurnL => self.cursor_rotated(Rotation::left ),
            TurnR => self.cursor_rotated(Rotation::right),
            MoveD => {
                let new_cursor = self.cursor_translated(Coord(0, -1));

                if new_cursor.is_none() {
                    self.place_cursor();
                    return self.clear_full_rows();
                }

                new_cursor
            },
            Slam => {
                self.cursor = self.cursor_projected();
                self.place_cursor();
                return self.clear_full_rows();
            },
        };
        
        if new_cursor.is_some() {
            self.cursor = new_cursor;
        }

        Status::Playing
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Cursor(pub &'static Template, pub Rotation, pub Coord<u16>);

impl Cursor {
    pub fn to_global(self) -> Piece {
        let Cursor(template, rotation, location) = self;
        template.apply(rotation, location)
    }
}

pub enum Status {
    Playing,
    Cleared(ArrayVec<[u16; BLOCK_COUNT]>),
    Lose
}

#[derive(Debug, Clone, Copy)]
pub enum Move {
    MoveL, MoveR,
    TurnL, TurnR,
    MoveD, Slam,
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn refill_cursor_empty() {
        let template = &tetrimino::L;
        let mut game = Game::new(Coord(8, 10), Duration::from_secs(1));
        assert!(game.cursor().is_none());

        game.refill_cursor(template);

        assert!(game.cursor().is_some());
        assert_eq!(
            Cursor(template, Rotation::default(), Coord(4, 9)),
            game.cursor().unwrap(),
        );
    }

    #[test]
    #[should_panic]
    fn refill_cursor_full() {
        let template = &tetrimino::L;
        let mut game = Game::new(Coord(8, 10), Duration::from_secs(1));
        game.refill_cursor(template);
        game.refill_cursor(template);
    }
}
