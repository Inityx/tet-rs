#![allow(dead_code)]

use std::{
    fmt::{self, Debug, Formatter},
    ops::Neg,
};
use super::rotate::{Rotate, Rotation};

#[derive(Clone, Copy, PartialEq)]
pub struct Coord<T>(pub T, pub T);

impl<T> Debug for Coord<T>
where T: Debug {
    fn fmt(&self, fmt: &mut Formatter) -> fmt::Result {
        let Coord(x, y) = self;
        write!(fmt, "({:?},{:?})", x, y)
    }
}

impl<T> Default for Coord<T>
where T: Default {
    fn default() -> Self {
        Coord(T::default(), T::default())
    }
}

macro_rules! convert {
    ($from:ty, $to:ty) => {
        impl From<Coord<$from>> for Coord<$to> {
            fn from(Coord(x, y): Coord<$from>) -> Coord<$to> {
                Coord(x as $to, y as $to)
            }
        }
    };
}

convert!(i8,  i16);
convert!(u16, i16);
convert!(i16, u16);
convert!(u16, usize);

impl<T> Rotate for Coord<T>
where T: Neg<Output=T> {
    fn rotate(self, rotation: Rotation) -> Self {
        let Coord(x, y) = self;

        match rotation.internal() {
            0 => Coord(x, y),
            1 => Coord(-y, x),
            2 => Coord(-x, -y),
            3 => Coord(y, -x),
            _ => unreachable!(),
        }
    }
}

macro_rules! binary_coord_op {
    ($trait:ident, $method:ident) => {
        impl<T> ::std::ops::$trait for Coord<T>
        where T: ::std::ops::$trait {
            type Output = Coord<<T as ::std::ops::$trait>::Output>;
            fn $method(self, Coord(rx, ry): Self) -> Self::Output {
                let Coord(sx, sy) = self;

                Coord(
                    sx.$method(rx),
                    sy.$method(ry)
                )
            }
        }
    };
}

binary_coord_op!(Add, add);
binary_coord_op!(Mul, mul);
binary_coord_op!(Div, div);
binary_coord_op!(Sub, sub);

macro_rules! binary_scalar_op {
    ($trait:ident, $method:ident) => {
        impl<T> ::std::ops::$trait<T> for Coord<T>
        where T: ::std::ops::$trait + Clone {
            type Output = Coord<<T as ::std::ops::$trait>::Output>;
            fn $method(self, rhs: T) -> Self::Output {
                let Coord(x, y) = self;

                Coord(
                    x.$method(rhs.clone()),
                    y.$method(rhs)
                )
            }
        }
    };
}

binary_scalar_op!(Add, add);
binary_scalar_op!(Mul, mul);
binary_scalar_op!(Div, div);
