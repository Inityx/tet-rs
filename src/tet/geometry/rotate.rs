use std::fmt;

#[derive(Clone, Copy, Default, PartialEq)]
pub struct Rotation(u8);

impl Rotation {
    pub fn internal(self) -> u8 {
        let Rotation(internal) = self;
        internal
    }

    pub fn left (self) -> Self { Rotation((self.internal() + 1) % 4) }
    pub fn right(self) -> Self { Rotation((self.internal() + 3) % 4) }
}

impl fmt::Debug for Rotation {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        write!(fmt, "{}°", u16::from(self.internal()) * 90)
    }
}

pub trait Rotate {
    fn rotate(self, rotation: Rotation) -> Self;
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn default() {
        let rotation = Rotation::default();
        assert_eq!(0, rotation.internal());
        assert_eq!("0°", format!("{:?}", rotation));
    }

    #[test]
    fn left() {
        let rotation = Rotation::default().left();
        assert_eq!(1, rotation.internal());
        assert_eq!("90°", format!("{:?}", rotation));
    }

    #[test]
    fn right() {
        let rotation = Rotation::default().right();
        assert_eq!(3, rotation.internal());
        assert_eq!("270°", format!("{:?}", rotation));
    }

    #[test]
    fn half() {
        let r1 = Rotation::default().left().left();
        let r2 = Rotation::default().right().right();
        assert_eq!(2, r1.internal());
        assert_eq!(2, r2.internal());
        assert_eq!(r1, r2);
        assert_eq!("180°", format!("{:?}", r1));
        assert_eq!("180°", format!("{:?}", r2));
    }

    #[test]
    fn overflow() {
        let r1 = Rotation::default().left().left().left().left();
        let r2 = Rotation::default().right().right().right().right();
        assert_eq!(0, r1.internal());
        assert_eq!(0, r2.internal());
    }
}
