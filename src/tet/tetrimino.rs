#![allow(dead_code)]

use crate::tet::{
    Color,
    geometry::{
        coord::Coord,
        rotate::{Rotation, Rotate},
    },
};

use std::fmt;

pub const BLOCK_COUNT: usize = 4;

// Tetrimino base
#[derive(Clone, Copy)]
pub struct Tetrimino<T>(pub char, pub BlockSet<T>, pub Color);

impl<T> PartialEq for Tetrimino<T> where T: PartialEq {
    fn eq(&self, Tetrimino(_, rb, _): &Self) -> bool {
        let Tetrimino(_, sb, _) = self;
        
        sb == rb
    }
}

impl<T> fmt::Debug for Tetrimino<T> {
    fn fmt(&self, fmt: &mut fmt::Formatter) -> fmt::Result {
        let Tetrimino(name, _, _) = self;
        write!(fmt, "{} piece", name)
    }
}

// Block container
#[derive(Clone, Copy, Default, PartialEq)]
pub struct BlockSet<T>(pub [Coord<T>;BLOCK_COUNT]);

impl<T> IntoIterator for BlockSet<T> {
    type Item = Coord<T>;
    type IntoIter = std::array::IntoIter<Self::Item, BLOCK_COUNT>;

    fn into_iter(self) -> Self::IntoIter {
        let BlockSet(blocks) = self;
        std::array::IntoIter::new(blocks)
    }
}

impl<T> ::std::iter::FromIterator<Coord<T>> for BlockSet<T> where T: Default {
    fn from_iter<I>(iter: I) -> Self
    where I: IntoIterator<Item=Coord<T>> {
        let mut blocks: [Coord<T>; BLOCK_COUNT] = Default::default();

        blocks
            .iter_mut()
            .zip(iter)
            .for_each(|(dest, src)| *dest = src);

        BlockSet(blocks)
    }
}

// Aux types
pub type Template = Tetrimino<i8>;
pub type Piece = Tetrimino<u16>;

impl Template {
    pub fn apply(&self, rotation: Rotation, offset: Coord<u16>) -> Piece {
        let Tetrimino(name, blocks, color) = self;
        let offset: Coord<i16> = offset.into();

        let blocks = blocks
            .into_iter()
            .map(|coord| coord.rotate(rotation))
            .map(Coord::<i16>::from)
            .map(|coord| offset + coord)
            .map(Coord::<u16>::from)
            .collect();
        
        Tetrimino(*name, blocks, *color)
    }
}

// Tetrimino templates
pub const L: Template = Tetrimino('L', BlockSet([Coord( 0,-1), Coord(0, 0), Coord( 1, 0), Coord( 2, 0)]), Color::Orange);
pub const J: Template = Tetrimino('J', BlockSet([Coord( 0,-1), Coord(0, 0), Coord(-1, 0), Coord(-2, 0)]), Color::Blue  );
pub const Z: Template = Tetrimino('Z', BlockSet([Coord(-1, 0), Coord(0, 0), Coord( 0,-1), Coord( 1,-1)]), Color::Green );
pub const S: Template = Tetrimino('S', BlockSet([Coord(-1,-1), Coord(0,-1), Coord( 0, 0), Coord( 1, 0)]), Color::Red   );
pub const T: Template = Tetrimino('T', BlockSet([Coord(-1, 0), Coord(0, 0), Coord( 1, 0), Coord( 0,-1)]), Color::Purple);
pub const O: Template = Tetrimino('O', BlockSet([Coord( 0, 0), Coord(0,-1), Coord( 1, 0), Coord( 1,-1)]), Color::Yellow);
pub const I: Template = Tetrimino('I', BlockSet([Coord(-1, 0), Coord(0, 0), Coord( 1, 0), Coord( 2, 0)]), Color::Cyan  );
 
pub const TEMPLATES: &[&Template] = &[&L, &J, &S, &Z, &T, &O, &I];

pub fn random() -> &'static Template {
    let selection = ::rand::random::<usize>() % TEMPLATES.len();
    TEMPLATES[selection]
}
